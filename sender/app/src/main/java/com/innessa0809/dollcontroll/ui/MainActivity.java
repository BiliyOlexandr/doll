package com.innessa0809.dollcontroll.ui;

import android.app.ActionBar;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;

import com.google.gson.Gson;
import com.innessa0809.dollcontroll.R;
import com.innessa0809.dollcontroll.data.BluetoothHandler;
import com.innessa0809.dollcontroll.data.FirebaseNotifyCreator;
import com.innessa0809.dollcontroll.data.remote.LoaderRemote;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.MediaController;
import android.widget.Toast;
import android.widget.VideoView;

import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private String TAG = "MainActivity";

    FirebaseNotifyCreator fc;
    Gson gson = new Gson();
    LoaderRemote loaderRemote;
    VideoView videoView;
    int idChoose, idChoose2, idDance, idHug, idJump, idTouch, idWave;
    int videoPlayed;
    //int choosed;
    private static final int REQUEST_CONNECT_DEVICE_SECURE = 1;
    private static final int REQUEST_CONNECT_DEVICE_INSECURE = 2;
    private static final int REQUEST_ENABLE_BT = 3;

    /**
     * Name of the connected device
     */
    private String mConnectedDeviceName = null;

    /**
     * String buffer for outgoing messages
     */
    private StringBuffer mOutStringBuffer;

    /**
     * Local Bluetooth adapter
     */
    private BluetoothAdapter mBluetoothAdapter = null;

    /**
     * Member object for the chat services
     */
    private BluetoothHandler mChatService = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Objects.requireNonNull(getSupportActionBar()).hide();
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        /*FirebaseDynamicLinks.getInstance()
                .getDynamicLink(getIntent())
                .addOnCompleteListener(new OnCompleteListener<PendingDynamicLinkData>() {
                    @Override
                    public void onComplete(@NonNull Task<PendingDynamicLinkData> task) {
                        if (task.isSuccessful()) {
                            Uri deepLink =  null;
                            if (task.getResult() != null) {
                                Log.e(TAG, "onComplete: 3");
                                deepLink = task.getResult().getLink();
                                String referral = deepLink.getQueryParameter("token");
                                Log.e(TAG, "onComplete: "+referral);
                                if (referral != null){
                                    App.getInstance().setToken(referral);
                                }
                            }
                        }
                    }
                });
        loaderRemote = new LoaderRemote();*/

        findViewById(R.id.btn_jump).setOnClickListener(this);
        findViewById(R.id.btn_dance).setOnClickListener(this);
        findViewById(R.id.btn_wave).setOnClickListener(this);
        findViewById(R.id.btn_hug).setOnClickListener(this);


        videoView = findViewById(R.id.videoView);
        //Creating MediaController
        MediaController mediaController = new MediaController(this);
        mediaController.setAnchorView(videoView);

        //specify the location of media file


        idChoose = R.raw.choose;
        idChoose2 = R.raw.chooseafter;
        idDance = R.raw.dance;
        idHug = R.raw.hug;
        idJump = R.raw.jump;
        idWave = R.raw.wave;
        idTouch = R.raw.sun_touch;

        //Setting MediaController and URI, then starting the videoView
        videoView.setMediaController(mediaController);

        videoView.requestFocus();
        videoView.setMediaController(null);
        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                if (videoPlayed == idChoose || videoPlayed == idChoose2){
                    startTimer();
                } else if (videoPlayed == idDance  || videoPlayed == idHug || videoPlayed == idJump || videoPlayed == idWave){
                    play(idChoose2);
                }
            }
        });

        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                if (videoPlayed == idTouch){
                    mp.setLooping(true);
                } else {
                    mp.setLooping(false);
                }
            }
        });

        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        // If the adapter is null, then Bluetooth is not supported
        if (mBluetoothAdapter == null) {
            Toast.makeText(this, getString(R.string.bluetooth_not_available), Toast.LENGTH_LONG).show();
        }
    }


    Timer timer;
    protected void startTimer(){
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        play(idTouch);
                    }
                });
            }
        }, 60000);
    }

    protected void stopTimer (){
        if (timer != null){
            timer.cancel();
            timer = null;
        }
    }



    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mChatService != null) {
            mChatService.stop();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        // Performing this check in onResume() covers the case in which BT was
        // not enabled during onStart(), so we were paused to enable it...
        // onResume() will be called when ACTION_REQUEST_ENABLE activity returns.
        if (mChatService != null) {
            // Only if the state is STATE_NONE, do we know that we haven't started already
            if (mChatService.getState() == BluetoothHandler.STATE_NONE) {
                // Start the Bluetooth chat services
                mChatService.start();
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        stopTimer();
    }

    protected void play(int idVideo){
        videoPlayed = idVideo;
        videoView.stopPlayback();
        Uri uri=Uri.parse("android.resource://"+getPackageName()+"/" + idVideo);
        videoView.setVideoURI(uri);
        videoView.start();
    }

    /*public void send(final String action)
    {
        final String token = App.getInstance().getToken();
        if (token == null){
            return;
    }


        Thread thread = new Thread() {
            @Override
            public void run() {
                fc = new FirebaseNotifyCreator(token, new FirebaseNotifyCreator.Data(action));
                String j = gson.toJson(fc);
                Log.e(TAG, "run: "+j);
                loaderRemote.requesPOST(getString(R.string.api_firebase), j);
            }
        };

        thread.start();
    }*/

    public void send(final String action){
        sendMessage(action);
    }

    @Override
    protected void onStart() {
        super.onStart();
        //1
        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
            // Otherwise, setup the chat session
        } else if (mChatService == null) {
            setupChat();
        }
        play(idTouch);
    }

    @Override
    public void onClick(View v) {
        stopTimer();

        //Log.e(TAG, "onClick: ");
        if (videoPlayed == idTouch){
            stopTimer();
            play(idChoose);
        } else if (videoPlayed == idChoose || videoPlayed == idChoose2){
            switch (v.getId()){
                case R.id.btn_jump:
                    play(idJump);
                    send(getString(R.string.jump));
                    break;
                case R.id.btn_dance:
                    play(idDance);
                    send(getString(R.string.dance));
                    break;
                case R.id.btn_wave:
                    play(idWave);
                    send(getString(R.string.wave));
                    break;
                case R.id.btn_hug:
                    play(idHug);
                    send(getString(R.string.hug));
                    break;
            }
        }
    }


    /**
     * Set up the UI and background operations for chat.
     */
    private void setupChat() {
        Log.d(TAG, "setupChat()");
        // Initialize the BluetoothHandler to perform bluetooth connections
        mChatService = new BluetoothHandler(mHandler);

        // Initialize the buffer for outgoing messages
        mOutStringBuffer = new StringBuffer("");
    }

    /**
     * Makes this device discoverable for 300 seconds (5 minutes).
     */
    private void ensureDiscoverable() {
        if (mBluetoothAdapter.getScanMode() !=
                BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE) {
            Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
            discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);
            startActivity(discoverableIntent);
        }
    }

    /**
     * Sends a message.
     *
     * @param message A string of text to send.
     */
    private void sendMessage(String message) {
        // Check that we're actually connected before trying anything
        if (mChatService.getState() != BluetoothHandler.STATE_CONNECTED) {
            Toast.makeText(this, R.string.not_connected, Toast.LENGTH_SHORT).show();
            return;
        }

        // Check that there's actually something to send
        if (message.length() > 0) {
            // Get the message bytes and tell the BluetoothHandler to write
            byte[] send = message.getBytes();
            mChatService.write(send);

            // Reset out string buffer to zero and clear the edit text field
            mOutStringBuffer.setLength(0);
        }
    }


    /**
     * Updates the status on the action bar.
     *
     * @param resId a string resource ID
     */
    private void setStatus(int resId) {

        final ActionBar actionBar = getActionBar();
        if (null != actionBar) {
            actionBar.setSubtitle(resId);
        }
    }

    /**
     * Updates the status on the action bar.
     *
     * @param subTitle status
     */
    private void setStatus(String subTitle) {
        final ActionBar actionBar = getActionBar();
        if (null != actionBar) {
            actionBar.setSubtitle(subTitle);
        }
    }

    /**
     * The Handler that gets information back from the BluetoothHandler
     */
    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case BluetoothHandler.MESSAGE_STATE_CHANGE:
                    switch (msg.arg1) {
                        case BluetoothHandler.STATE_CONNECTED:
                            setStatus(getApplicationContext().getString(R.string.title_connected_to)+ mConnectedDeviceName);
                            //mConversationArrayAdapter.clear();
                            break;
                        case BluetoothHandler.STATE_CONNECTING:
                            setStatus(R.string.title_connecting);
                            break;
                        case BluetoothHandler.STATE_LISTEN:
                        case BluetoothHandler.STATE_NONE:
                            setStatus(R.string.title_not_connected);
                            break;
                    }
                    break;
                case BluetoothHandler.MESSAGE_WRITE:
                    byte[] writeBuf = (byte[]) msg.obj;
                    // construct a string from the buffer
                    String writeMessage = new String(writeBuf);
                    //mConversationArrayAdapter.add("Me:  " + writeMessage);
                    break;
                case BluetoothHandler.MESSAGE_READ:
                    byte[] readBuf = (byte[]) msg.obj;
                    // construct a string from the valid bytes in the buffer
                    String readMessage = new String(readBuf, 0, msg.arg1);
                    //mConversationArrayAdapter.add(readMessage);
                    break;
                case BluetoothHandler.MESSAGE_DEVICE_NAME:
                    // save the connected device's name
                    mConnectedDeviceName = msg.getData().getString(BluetoothHandler.DEVICE_NAME);
                    Toast.makeText(MainActivity.this, getString(R.string.connected_to)+" "+ mConnectedDeviceName, Toast.LENGTH_SHORT).show();

                    break;
                case BluetoothHandler.MESSAGE_TOAST:
                    Toast.makeText(MainActivity.this, msg.getData().getString(BluetoothHandler.TOAST),
                            Toast.LENGTH_SHORT).show();

                    break;
            }
        }
    };

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CONNECT_DEVICE_SECURE:
                // When DeviceListActivity returns with a device to connect
                if (resultCode == Activity.RESULT_OK) {
                    connectDevice(data, true);
                }
                break;
            case REQUEST_CONNECT_DEVICE_INSECURE:
                // When DeviceListActivity returns with a device to connect
                if (resultCode == Activity.RESULT_OK) {
                    connectDevice(data, false);
                }
                break;
            case REQUEST_ENABLE_BT:
                // When the request to enable Bluetooth returns
                if (resultCode == Activity.RESULT_OK) {
                    // Bluetooth is now enabled, so set up a chat session
                    setupChat();
                } else {
                    // User did not enable Bluetooth or an error occurred
                    Log.d(TAG, "BT not enabled");
                    Toast.makeText(this, R.string.bt_not_enabled_leaving,
                            Toast.LENGTH_SHORT).show();
                    //getActivity().finish();
                }
        }
    }

    /**
     * Establish connection with other device
     *
     * @param data   An {@link Intent} with {@link DeviceListActivity#EXTRA_DEVICE_ADDRESS} extra.
     * @param secure Socket Security type - Secure (true) , Insecure (false)
     */
    private void connectDevice(Intent data, boolean secure) {
        // Get the device MAC address
        String address = data.getExtras()
                .getString(DeviceListActivity.EXTRA_DEVICE_ADDRESS);
        // Get the BluetoothDevice object
        BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
        // Attempt to connect to the device
        mChatService.connect(device, secure);
    }

}
