package com.innessa0809.dollcontroll.data;



public class FirebaseNotifyCreator {

    String to;
    Data data;

    public FirebaseNotifyCreator(String token, Data data) {
        to = token;
        this.data = data;
    }


    public static class Data{
        String action;
        public Data(String action){
           this.action = action;
        }
    }

}
