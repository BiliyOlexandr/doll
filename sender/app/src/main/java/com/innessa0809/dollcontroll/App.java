package com.innessa0809.dollcontroll;

import android.app.Application;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.DrawableRes;

import com.google.firebase.FirebaseApp;
import com.innessa0809.dollcontroll.data.local.PreferenceHandler;

public class App extends Application {

    private static App instance;
    public static App getInstance() {
        return instance;
    }

    private PreferenceHandler local;
    String token;
    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        FirebaseApp.initializeApp(this);
        local = new PreferenceHandler(this);
        if (local.isExist("token")){
            token = local.getStr("token");
        }
    }

    public String getToken() {
        return token;
    }

    public void showToast(String string, @DrawableRes int idIcon){
        Toast toast = Toast.makeText(this, string, Toast.LENGTH_SHORT);
        View toastView = toast.getView(); // This'll return the default View of the Toast.
        TextView toastMessage = toastView.findViewById(android.R.id.message);
        toastMessage.setCompoundDrawablesWithIntrinsicBounds(idIcon, 0, 0, 0);
        toastMessage.setGravity(Gravity.CENTER);
        toastMessage.setCompoundDrawablePadding(16);
        toast.show();
    }

    public void setToken(String token) {
        this.token = token;
        local.save("token", token);
    }
}
