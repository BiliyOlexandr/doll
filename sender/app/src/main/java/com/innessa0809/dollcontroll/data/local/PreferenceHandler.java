package com.innessa0809.dollcontroll.data.local;

import android.content.Context;
import android.content.SharedPreferences;

public class PreferenceHandler {
    final static String FILE_NAME = "main";

    private SharedPreferences preferences;

    public PreferenceHandler(Context context) {
        preferences = context.getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE);
    }

    public void save(String key, String value){
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(key, value);
        editor.apply();
        editor.commit();
    }

    public String getStr(String key){
        return preferences.getString(key, null);
    }

    public void save(String key, long value){
        SharedPreferences.Editor editor = preferences.edit();
        editor.putLong(key, value);
        editor.apply();
        editor.commit();
    }

    public long getLong(String key){
        return preferences.getLong(key, 0);
    }

    public SharedPreferences getPreferences(){
        return preferences;
    }


    public boolean isExist(String key){
        return preferences.contains(key);
    }

    public void remove(String key){
        SharedPreferences.Editor editor = preferences.edit();
        editor.remove(key);
        editor.apply();
        editor.commit();
    }
}
