package com.innessa0809.dolldance.ui;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ActionBar;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.ShortDynamicLink;
import com.innessa0809.dolldance.App;
import com.innessa0809.dolldance.BluetoothState;
import com.innessa0809.dolldance.R;
import com.innessa0809.dolldance.data.BluetoothHandler;
import com.innessa0809.dolldance.service.VidgetService;
import com.innessa0809.dolldance.service.VidgetService2;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {

    private static final int REQUEST_CONNECT_DEVICE_SECURE = 1;
    private static final int REQUEST_CONNECT_DEVICE_INSECURE = 2;
    private static final int REQUEST_ENABLE_BT = 3;
    String TAG = "MainActivity";

    TextView tvState;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                /*if (App.getInstance().isCheckPermissionOverlay()){
                    Intent intent = new Intent(MainActivity.this, VidgetService2.class);
                    startService(intent);
                } else {
                    getPermissionOverlay();
                }*/

                //1
                if (App.getInstance().isCheckPermissionOverlay()){
                    BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
                    if (!mBluetoothAdapter.isEnabled()) {
                        Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                        startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
                        // Otherwise, setup the chat session
                    } else{
                        App.getInstance().startVidgetService();
                        Intent serverIntent = new Intent(MainActivity.this, DeviceListActivity.class);
                        startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE_SECURE);
                    }
                } else {
                    getPermissionOverlay();
                }
            }
        });
        findViewById(R.id.button2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                App.getInstance().stopVidgetService();
            }
        });
        tvState = findViewById(R.id.textView);
    }


    public final static int REQUEST_CODE = 5463;
    public void getPermissionOverlay() {
        Intent intent = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION, Uri.parse("package:" + getPackageName()));
            startActivityForResult(intent, REQUEST_CODE);
            finish();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        //2
        if (VidgetService.isInstanceCreated()){
            tvState.setText(VidgetService.getInstance().getStateConnected());
        } else {
            tvState.setText(getString(R.string.not_connected));
        }

        VidgetService.bluetoothState = new BluetoothState() {
            @Override
            public void stateChange(String state) {
                tvState.setText(state);
            }
        };
    }

    @Override
    protected void onStop() {
        super.onStop();
        VidgetService.bluetoothState = null;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!VidgetService.isInstanceCreated()){return;}

        switch (requestCode) {
            case REQUEST_CONNECT_DEVICE_SECURE:
                // When DeviceListActivity returns with a device to connect
                if (resultCode == Activity.RESULT_OK) {
                    VidgetService.serviceInterface.connectDevice(data, true);
                }
                break;
            case REQUEST_CONNECT_DEVICE_INSECURE:
                // When DeviceListActivity returns with a device to connect
                if (resultCode == Activity.RESULT_OK) {
                    VidgetService.serviceInterface.connectDevice(data, false);
                }
                break;
            case REQUEST_ENABLE_BT:
                // When the request to enable Bluetooth returns
                if (resultCode == Activity.RESULT_OK) {
                    // Bluetooth is now enabled, so set up a chat session
                    VidgetService.serviceInterface.setupChat();
                } else {
                    // User did not enable Bluetooth or an error occurred
                    Log.d(TAG, "BT not enabled");
                    Toast.makeText(this, R.string.bt_not_enabled_leaving,
                            Toast.LENGTH_SHORT).show();
                    //getActivity().finish();
                }
        }
    }


    private void shareToken(Uri link){
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_SUBJECT, "Token");
        intent.putExtra(Intent.EXTRA_TEXT, link.toString());
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(Intent.createChooser(intent, "Token"));
        }
    }


    public void createDynamicLink(){
        if (App.getInstance().getToken() == null){
            return;
        }

        Task<ShortDynamicLink> shortLinkTask = FirebaseDynamicLinks.getInstance().createDynamicLink()
                .setLink(getLink())
                .setDomainUriPrefix("https://innessa0809.page.link")
                .buildShortDynamicLink()
                .addOnCompleteListener(this, new OnCompleteListener<ShortDynamicLink>() {
                    @Override
                    public void onComplete(@NonNull Task<ShortDynamicLink> task) {
                        if (task.isSuccessful()) {
                            Uri shortLink = task.getResult().getShortLink();
                            shareToken(shortLink);
                        } else {

                        }
                    }
                });
    }

    public Uri getLink() {
        Uri.Builder builder = new Uri.Builder()
                .scheme("https")
                .authority("play.google.com")
                .path("/store/apps/details")
                .appendQueryParameter("id", "com.innessa0809.dollcontroll")
                .appendQueryParameter("token", App.getInstance().getToken());
        return builder.build();
    }

}
