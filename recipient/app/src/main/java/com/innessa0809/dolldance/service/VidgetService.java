package com.innessa0809.dolldance.service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.content.Context;
import android.graphics.PixelFormat;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;

import com.innessa0809.dolldance.App;
import com.innessa0809.dolldance.BluetoothState;
import com.innessa0809.dolldance.R;
import com.innessa0809.dolldance.data.BluetoothHandler;
import com.innessa0809.dolldance.ui.DeviceListActivity;
import com.innessa0809.dolldance.ui.MainActivity;

import java.util.Timer;
import java.util.TimerTask;

public class VidgetService extends Service {

    String TAG = "VidgetService";
    private static VidgetService instance = null;
    public static boolean isInstanceCreated() {
        return instance != null;
    }

    public static VidgetService getInstance() {
        return instance;
    }

    public static final String NOTIFICATION_CHANNEL_ID = "com.innessa0809.dolldance.vidget";
    public static NotificationChannel notificationChannel;


    public static BluetoothState bluetoothState;
    public static ServiceInterface serviceInterface = new ServiceInterface() {
        @Override
        public void connectDevice(Intent data, boolean sec) {
            if (instance != null){
                instance.connectDevice(data, sec);
            }
        }

        @Override
        public void setupChat() {
            if (instance != null){
                instance.setupChat();
            }
        }
    };


    protected String stateConnected;

    public String getStateConnected() {
        return stateConnected;
    }

    /**
     * Name of the connected device
     */
    public String mConnectedDeviceName = null;

    /**
     * String buffer for outgoing messages
     */
    public StringBuffer mOutStringBuffer;

    /**
     * Local Bluetooth adapter
     */
    public BluetoothAdapter mBluetoothAdapter = null;

    /**
     * Member object for the chat services
     */
    public BluetoothHandler bluetoothHandler = null;


    public VidgetService() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        // If the adapter is null, then Bluetooth is not supported
        if (mBluetoothAdapter == null) {
            Toast.makeText(this, getString(R.string.bluetooth_not_available), Toast.LENGTH_LONG).show();
        }
        stateConnected = getString(R.string.title_not_connected);


        setupChat();
    }

    @Override
    public IBinder onBind(Intent intent) {
       return null;
    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.e(TAG, "onStartCommand: 1");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            if (notificationChannel == null){
                notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "Notification", NotificationManager.IMPORTANCE_HIGH);
                notificationChannel.setLockscreenVisibility(Notification.VISIBILITY_SECRET);
                notificationChannel.enableVibration(true);
                notificationChannel.setSound(null, null);

                NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                assert manager != null;
                manager.createNotificationChannel(notificationChannel);

                Log.e(TAG, "onStartCommand: 2");
                //re.. to activity
                Intent notificationIntent = new Intent(this, MainActivity.class);
                //intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                        notificationIntent, 0);


                Notification.Builder builder = new Notification.Builder(this, NOTIFICATION_CHANNEL_ID)
                        .setContentIntent(pendingIntent)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentText("Notification working");

                Log.e(TAG, "onStartCommand: 3");
                startForeground(5, builder.build());
            }
        } else {
            NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle("Notification working")
                    .setContentText("Notification working");


            startForeground(5, builder.build());
        }

        startBluetooth();
        Log.e(TAG, "onStartCommand: 4");
        return START_STICKY;
    }


    public void startBluetooth(){
        if (bluetoothHandler != null) {
            // Only if the state is STATE_NONE, do we know that we haven't started already
            if (bluetoothHandler.getState() == BluetoothHandler.STATE_NONE) {
                // Start the Bluetooth chat services
                bluetoothHandler.start();
            }
        }
    }


    //private WindowManager windowManager;
    //private TextView view;

    /**
     * Establish connection with other device
     *
     * @param data   An {@link Intent} with {@link DeviceListActivity#EXTRA_DEVICE_ADDRESS} extra.
     * @param secure Socket Security type - Secure (true) , Insecure (false)
     */
    public void connectDevice(Intent data, boolean secure) {
        // Get the device MAC address
        String address = data.getExtras().getString(DeviceListActivity.EXTRA_DEVICE_ADDRESS);
        // Get the BluetoothDevice object
        BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
        // Attempt to connect to the device
        bluetoothHandler.connect(device, secure);
    }

    /**
     * Set up the UI and background operations for chat.
     */
    public void setupChat() {
        Log.d(TAG, "setupChat()");

        // Initialize the BluetoothHandler to perform bluetooth connections
        bluetoothHandler = new BluetoothHandler(mHandler);

        // Initialize the buffer for outgoing messages
        mOutStringBuffer = new StringBuffer("");
    }

    /**
     * Makes this device discoverable for 300 seconds (5 minutes).
     */
    private void ensureDiscoverable() {
        if (mBluetoothAdapter.getScanMode() !=
                BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE) {
            Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
            discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);
            startActivity(discoverableIntent);
        }
    }

    /**
     * Sends a message.
     *
     * @param message A string of text to send.
     */
    private void sendMessage(String message) {
        // Check that we're actually connected before trying anything
        if (bluetoothHandler.getState() != BluetoothHandler.STATE_CONNECTED) {
            Toast.makeText(this, R.string.not_connected, Toast.LENGTH_SHORT).show();
            return;
        }

        // Check that there's actually something to send
        if (message.length() > 0) {
            // Get the message bytes and tell the BluetoothHandler to write
            byte[] send = message.getBytes();
            bluetoothHandler.write(send);

            // Reset out string buffer to zero and clear the edit text field
            mOutStringBuffer.setLength(0);
        }
    }


    /**
     * Updates the status on the action bar.
     *
     * @param resId a string resource ID
     */
    private void setStatus(int resId) {
        Toast.makeText(VidgetService.this, getString(resId), Toast.LENGTH_SHORT).show();
    }

    /**
     * Updates the status on the action bar.
     *
     * @param subTitle status
     */
    private void setStatus(String subTitle) {
        Toast.makeText(VidgetService.this, subTitle, Toast.LENGTH_SHORT).show();
    }

    /**
     * The Handler that gets information back from the BluetoothHandler
     */


    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case BluetoothHandler.MESSAGE_STATE_CHANGE:
                    switch (msg.arg1) {
                        case BluetoothHandler.STATE_CONNECTED:
                            stateConnected = getString(R.string.title_connected_to)+ mConnectedDeviceName;
                            //setStatus(getApplicationContext().getString(R.string.title_connected_to)+ mConnectedDeviceName);
                            //mConversationArrayAdapter.clear();
                            break;
                        case BluetoothHandler.STATE_CONNECTING:
                            stateConnected = getString(R.string.title_connecting);
                            //setStatus(R.string.title_connecting);
                            break;
                        case BluetoothHandler.STATE_LISTEN:
                        case BluetoothHandler.STATE_NONE:
                            stateConnected = getString(R.string.title_not_connected);
                            //setStatus(R.string.title_not_connected);
                            break;
                    }
                    if (bluetoothState != null){
                        bluetoothState.stateChange(stateConnected);
                    }
                    break;
                case BluetoothHandler.MESSAGE_WRITE:
                    byte[] writeBuf = (byte[]) msg.obj;
                    // construct a string from the buffer
                    String writeMessage = new String(writeBuf);
                    //mConversationArrayAdapter.add("Me:  " + writeMessage);
                    break;
                case BluetoothHandler.MESSAGE_READ:
                    byte[] readBuf = (byte[]) msg.obj;
                    // construct a string from the valid bytes in the buffer
                    String readMessage = new String(readBuf, 0, msg.arg1);
                    //mConversationArrayAdapter.add(readMessage);
                    show(readMessage);
                    break;
                case BluetoothHandler.MESSAGE_DEVICE_NAME:
                    // save the connected device's name
                    mConnectedDeviceName = msg.getData().getString(BluetoothHandler.DEVICE_NAME);
                    Toast.makeText(VidgetService.this, getString(R.string.connected_to)+" "+ mConnectedDeviceName, Toast.LENGTH_SHORT).show();

                    break;
                case BluetoothHandler.MESSAGE_TOAST:
                    Toast.makeText(VidgetService.this, msg.getData().getString(BluetoothHandler.TOAST),
                            Toast.LENGTH_SHORT).show();

                    break;
            }
        }
    };

    /*public void show(final String action){
        if (timer != null){return;}
        startTimer();
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                App.getInstance().showToast(action, R.mipmap.ic_launcher);
            }
        });
    }*/


    public static Timer timer;
    void startTimer(){
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                stopTimer();
            }
        }, 10000);
    }

    void stopTimer(){
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
        removeView();
    }

    @Override
    public void onDestroy() {
        stopTimer();
        instance = null;
        notificationChannel = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            ((NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE)).deleteNotificationChannel(NOTIFICATION_CHANNEL_ID);
        }

        if (bluetoothHandler != null) {
            bluetoothHandler.stop();
        }

        bluetoothState = null;
        removeView();
        super.onDestroy();
    }


    private void removeView(){
        try{
            if (view.getParent() != null) {
                windowManager.removeView(view);
            }
        } catch(IllegalArgumentException e){
            e.printStackTrace();
        }
    }


    WindowManager windowManager;
    View view;
    public void show(final String action){
        if (timer != null){return;}
        startTimer();
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                view = LayoutInflater.from(VidgetService.this).inflate(R.layout.notify, null);
                TextView tv = view.findViewById(R.id.textView2);
                tv.setText(action);

                windowManager = (WindowManager)getSystemService(WINDOW_SERVICE);
                //here is all the science of params
                final WindowManager.LayoutParams myParams;

                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                    myParams = new WindowManager.LayoutParams(
                            WindowManager.LayoutParams.MATCH_PARENT,
                            WindowManager.LayoutParams.MATCH_PARENT,
                            WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY ,
                            WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                            PixelFormat.TRANSLUCENT);
                } else {
                    myParams = new WindowManager.LayoutParams(
                            WindowManager.LayoutParams.MATCH_PARENT,
                            WindowManager.LayoutParams.MATCH_PARENT,
                            WindowManager.LayoutParams.TYPE_PHONE,
                            WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                            PixelFormat.TRANSLUCENT);
                }
                myParams.gravity =  Gravity.TOP;
                //myParams.gravity = Gravity.CENTER_VERTICAL | Gravity.BOTTOM;
                windowManager.addView(view, myParams);
            }
        });
    }

    public interface ServiceInterface {
        void connectDevice(Intent data, boolean sec);
        void setupChat();
    }
}
