package com.innessa0809.dolldance.service;

import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.innessa0809.dolldance.App;

import java.util.Map;
import java.util.Objects;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    String TAG = "MyFirebaseMessagingService";


    private static final String KEY_DANCE = "work_service";


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.e(TAG, "onMessageReceived: "+ remoteMessage.getData().toString());
        if (remoteMessage.getData().size()>0) {
            if (!remoteMessage.getData().containsKey("action")){return;}
            App.getInstance().newAction(remoteMessage.getData().get("action"));
        } else {
            //notification
        }
    }


    @Override
    public void onNewToken(String token) {
        super.onNewToken(token);
    }

}
