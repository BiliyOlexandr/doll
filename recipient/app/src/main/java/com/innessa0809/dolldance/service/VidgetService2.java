package com.innessa0809.dolldance.service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.PixelFormat;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.core.app.NotificationCompat;

import com.innessa0809.dolldance.R;
import com.innessa0809.dolldance.ui.MainActivity;

import java.util.Timer;
import java.util.TimerTask;

public class VidgetService2 extends Service {



    public VidgetService2() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public IBinder onBind(Intent intent) {
       return null;
    }
    String TAG = "VidgetService";
    public static final String NOTIFICATION_CHANNEL_ID = "com.innessa0809.dolldance.vidget";
    public static NotificationChannel notificationChannel;

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.e(TAG, "onStartCommand: 1");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            if (notificationChannel == null){
                notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "Notification", NotificationManager.IMPORTANCE_HIGH);
                notificationChannel.setLockscreenVisibility(Notification.VISIBILITY_SECRET);
                notificationChannel.enableVibration(true);
                notificationChannel.setSound(null, null);

                NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                assert manager != null;
                manager.createNotificationChannel(notificationChannel);

                Log.e(TAG, "onStartCommand: 2");
                //re.. to activity
                Intent notificationIntent = new Intent(this, MainActivity.class);
                //intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                        notificationIntent, 0);


                Notification.Builder builder = new Notification.Builder(this, NOTIFICATION_CHANNEL_ID)
                        .setContentIntent(pendingIntent)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentText("Notification working");

                Log.e(TAG, "onStartCommand: 3");
                startForeground(5, builder.build());
            }
        } else {
            NotificationCompat.Builder builder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle("Notification working")
                    .setContentText("Notification working");


            startForeground(5, builder.build());

        }

        show("dsfgd");
        Log.e(TAG, "onStartCommand: 4");
        return START_STICKY;
    }


    public int dpToPx(int dps) {
        float scale = this.getResources().getDisplayMetrics().density;
        return (int) (dps * scale + 0.5f);
    }



    @Override
    public void onDestroy() {
        notificationChannel = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            ((NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE)).deleteNotificationChannel(NOTIFICATION_CHANNEL_ID);
        }
        removeView();

        super.onDestroy();
    }


    private void removeView(){
        try{
            if (view.getParent() != null) {
                windowManager.removeView(view);
            }
        } catch(IllegalArgumentException e){
            e.printStackTrace();
        }
    }

    public static Timer timer;
    void startTimer(){
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                stopTimer();
            }
        }, 5000);
    }

    void stopTimer(){
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
        removeView();
    }

    WindowManager windowManager;
    View view;
    public void show(final String action){
        if (timer != null){return;}
        startTimer();
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                view = LayoutInflater.from(VidgetService2.this).inflate(R.layout.notify, null);
                TextView tv = view.findViewById(R.id.textView2);
                tv.setText(action);

                windowManager = (WindowManager)getSystemService(WINDOW_SERVICE);
                //here is all the science of params
                final WindowManager.LayoutParams myParams;

                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                    myParams = new WindowManager.LayoutParams(
                            WindowManager.LayoutParams.MATCH_PARENT,
                            WindowManager.LayoutParams.MATCH_PARENT,
                            WindowManager.LayoutParams.TYPE_APPLICATION_OVERLAY ,
                            WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                            PixelFormat.TRANSLUCENT);
                } else {
                    myParams = new WindowManager.LayoutParams(
                            WindowManager.LayoutParams.MATCH_PARENT,
                            WindowManager.LayoutParams.MATCH_PARENT,
                            WindowManager.LayoutParams.TYPE_PHONE,
                            WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE,
                            PixelFormat.TRANSLUCENT);
                }
                myParams.gravity =  Gravity.TOP;
                //myParams.gravity = Gravity.CENTER_VERTICAL | Gravity.BOTTOM;
                windowManager.addView(view, myParams);
            }
        });
    }
}
