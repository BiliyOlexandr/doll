package com.innessa0809.dolldance;

import android.app.Application;
import android.content.Intent;
import android.os.Build;
import android.os.CountDownTimer;
import android.provider.Settings;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.innessa0809.dolldance.service.VidgetService;

import java.util.Objects;

public class App extends Application {

    String TAG = "App";
    private static App instance;

    public static App getInstance() {
        return instance;
    }

    private String token;
    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        Log.e(TAG, "onCreate: ");
        FirebaseApp.initializeApp(this);
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        Log.e(TAG, "onComplete: 1");
                        if (!task.isSuccessful()) {
                            return;
                        }
                        token = Objects.requireNonNull(task.getResult()).getToken();
                        Log.e(TAG, "onComplete: 2"+token);
                    }
                });
    }


    public String getToken() {
        return token;
    }

    public void showToast(String string, @DrawableRes int idIcon){
        final Toast toast = Toast.makeText(this, string, Toast.LENGTH_LONG);
        View toastView = toast.getView(); // This'll return the default View of the Toast.
        TextView toastMessage = toastView.findViewById(android.R.id.message);
        toastMessage.setTextSize(28);
        toastMessage.setPadding(40,20,40,20);
        //toastMessage.setCompoundDrawablesWithIntrinsicBounds(idIcon, 0, 0, 0);
        //toastMessage.setGravity(Gravity.CENTER);
        toastMessage.setCompoundDrawablePadding(16);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
        new CountDownTimer(5000, 1000)
        {
            public void onTick(long millisUntilFinished) {toast.show();}
            public void onFinish() {toast.show();}

        }.start();
    }

    public void newAction(String action){
        if (VidgetService.isInstanceCreated()){
            VidgetService.getInstance().show(action);
        }
    }


    public void startVidgetService() {
        if (VidgetService.isInstanceCreated()){return;}
        Log.e(TAG, "startVidgetService: 1");
        Intent intent = new Intent(this, VidgetService.class);
        Log.e(TAG, "startVidgetService: 2");
        startService(intent);

    }

    public void stopVidgetService(){
        stopService(new Intent(this, VidgetService.class));
    }


    public boolean isCheckPermissionOverlay() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M){ return true; }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O_MR1) {
            return Settings.canDrawOverlays(this);
        } else {
            if (Settings.canDrawOverlays(this)) {
                return true;
            } else {
                return false;
            }
        }
    }
}
