package com.innessa0809.dolldance;

import com.innessa0809.dolldance.data.BluetoothHandler;

public interface BluetoothState {
    void stateChange(String state);
}
